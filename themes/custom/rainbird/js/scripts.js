(function ($, Drupal) {
  Drupal.behaviors.myModuleBehavior = {
    attach: function (context, settings) {
      $('body', context).once('myCustomBehavior').each(function () {
        $('.form-type-radio .control-label').each(function() {
          $(this).click(function () {
            $(this).parent().parent().find('.form-type-radio .control-label').removeClass('active');
            $(this).addClass('active');
          });
        });

        $('.form-radio').once('myCustomBehaviorTwo').each(function () {
          if ($(this).is(':checked')) {
            $(this).parent().parent().addClass('active');
          }
        });
      });

      $('.form-type-radio label').textfill({
        maxFontPixels: 24,
        minFontPixels: 12,
      });
    }
  };
})(jQuery, Drupal);
