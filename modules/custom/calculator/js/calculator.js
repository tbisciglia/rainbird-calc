/**
 * @file
 * Provides JavaScript for Calculator module.
 */

(function ($, Drupal) {

  /**
   * Various JS enhancements.
   */
  Drupal.behaviors.calculator = {
    attach: function (context, settings) {
      var factor = 10.7639104;

      function changeUnits() {
        var units_choice = $('#edit-units').val();
        //console.log('Units: ' + units_choice);
        //console.log($('#edit-4-metric'));
        //console.log($('#edit-4-metric:visible'));
        if (units_choice == 'English' && $('#edit-4-metric:visible').length == 1) {
          //console.log('switch to English');
          $('.field_english').parents('div.form-item').show();
          $('.field_metric').parents('div.form-item').hide();
          $('.units').each(function() {
            var units = $(this).html().trim().toLowerCase();
            var num = $(this).prev();
            if (!num.hasClass('unit-convert')) {
              num = $(this).next();
              if (!num.hasClass('unit-convert')) {
                num = null;
              }
            }
            if (num !== null) {
              var num_old = parseFloat(num.html());
              var num_new;
              switch (units) {
                case 'cm':
                  units = 'in';
                  num_new = (num_old / 2.54).toFixed(2);
                  break;
                case 'mm/hr':
                  units = 'in/hr';
                  num_new = (num_old / 25.4).toFixed(2);
                  break;
                case 'm':
                  units = 'ft';
                  num_new = (num_old / 0.3048).toFixed(2);
                  break;
                case 'sq m':
                  units = 'sq ft';
                  num_new = (num_old / 0.3048 / 0.3048).toFixed(2);
                  break;
                case 'lph':
                  units = 'GPH';
                  num_new = (num_old / 3.78541).toFixed(2);
                  break;
                case 'lpm':
                  units = 'GPM';
                  num_new = (num_old / 3.78541).toFixed(2);
                  break;
                case 'bars':
                  units = 'psi';
                  num_new = (num_old / 0.0689476).toFixed(2);
                  break;
              }
              num.html(num_new);
              $(this).html(units);
            }
          });
        }
        else if (units_choice == 'Metric' && $('#edit-4:visible').length == 1) {
          //console.log('switch to Metric');
          $('.field_english').parents('div.form-item').hide();
          $('.field_metric').parents('div.form-item').show();
          $('.units').each(function() {
            var units = $(this).html().trim().toLowerCase();
            var num = $(this).prev();
            if (!num.hasClass('unit-convert')) {
              num = $(this).next();
              if (!num.hasClass('unit-convert')) {
                num = null;
              }
            }
            if (num !== null) {
              var num_old = parseFloat(num.html());
              var num_new;
              switch (units) {
                case 'in':
                  units = 'cm';
                  num_new = (num_old * 2.54).toFixed(2);
                  break;
                case 'in/hr':
                  units = 'mm/hr';
                  num_new = (num_old * 25.4).toFixed(2);
                  break;
                case 'ft':
                  units = 'm';
                  num_new = (num_old * 0.3048).toFixed(2);
                  break;
                case 'sq ft':
                  units = 'sq m';
                  num_new = (num_old * 0.3048 * 0.3048).toFixed(2);
                  break;
                case 'gph':
                  units = 'LPH';
                  num_new = (num_old * 3.78541).toFixed(2);
                  break;
                case 'gpm':
                  units = 'LPM';
                  num_new = (num_old * 3.78541).toFixed(2);
                  break;
                case 'psi':
                  units = 'bars';
                  num_new = (num_old * 0.0689476).toFixed(2);
                  break;
              }
              num.html(num_new);
              $(this).html(units);
            }
          });
        }
      }

      changeUnits();

      $('#edit-units').change(function() {
        changeUnits();
      });

      $('#edit-4').change(function() {
        $('#edit-4-metric').val(Math.round(parseFloat($('#edit-4').val()) / factor * 100) / 100);
      });
      $('#edit-4-metric').change(function() {
        $('#edit-4').val(Math.round(parseFloat($('#edit-4-metric').val()) * factor * 100) / 100);
      });

      if ($('input[name="5"]').val() != 16) {
        $('#edit-6--wrapper').hide();
      }

      $('#edit-5-15').click(function(e) {
        $('#edit-6--wrapper').hide();
      });
      $('#edit-5-16').click(function(e) {
        $('#edit-6--wrapper').show();
      });

      $('.max-gpm').change(function(e) {
        var max_gpm = parseFloat($('.max-gpm').val());
        var gpm = parseFloat($('.gpm').html());
        $('.num-kits').html(Math.ceil(gpm / max_gpm));
      });

      // Area calculator functions

      $('a.sf-calc').click(function(e) {
        e.preventDefault();
        if ($('#area-calculator').html() == '') {
          $.ajax({
            type: 'GET',
            url: '/modules/custom/calculator/includes/calc.html',
            success: function(data) {
              $('#area-calculator').html(data);
              Drupal.attachBehaviors($('#area-calculator'));
              $('#area-calculator td.dataheader span#added-areas').hide();
              $('#area-calculator .total-row').hide();
              $('#area-calculator input#clear-areas').hide();
            }
          });
        }
      });

      $('input#area-square-width, input#area-square-height').focus(function() {
        $('input#area-square').click();
      });

      $('input#area-circle-radius, select#area-circle-angle').focus(function() {
        $('input#area-circle').click();
      });

      $('input#area-triangle-width, input#area-triangle-height').focus(function() {
        $('input#area-triangle').click();
      });

      $('#area-calculator input[type="button"]').click(function(e) {
        e.preventDefault();
        var id = $(this).attr('id');
        switch(id) {
          case 'update-area':
            $('table#area_calculator input[type="radio"]:checked').each(function() {
              var area;
              var added_area_content;
              var area_total = 0;
              var units;
              if ($('select#edit-units').val() == 'English') {
                units = 'ft';
              }
              else {
                units = 'm';
              }
              switch ($(this).attr('id')) {
                case 'area-square':
                  var l = $('input#area-square-width').val();
                  var h = $('input#area-square-height').val();
                  var rows = $('.subtotal-row').length;
                  if ($.isNumeric(l) && $.isNumeric(h)) {
                    area = l * h;
                    added_area_content = '<tr class="subtotal-row" id="srow-' + rows + '"><td class="medium" style="width:30px;white-space:nowrap;"><img src="/modules/custom/calculator/images/area-square.jpg" width="30" height="30" border="0">&nbsp;</td><td class="medium" style="width:110px;"><span class="unit-convert">' + l + '</span> <span class="units">' + units + '</span> x <span class="unit-convert">' + h + '</span> <span class="units">' + units + '</span></td><td class="medium area-subtotal" align="right" style="width:110px;"><span class="unit-convert">' + area.toFixed(2) + '</span> <span class="units">sq ' + units + '</span></td></tr>'
                    $('table#areas > tbody .total-row.spacer').before(added_area_content);
                    $('.total-row span.units').html('sq ' + units);
                    $('#area-calculator td.dataheader span#added-areas').show();
                    $('#area-calculator .total-row').show();
                    $('#area-calculator input#clear-areas').show();
                    var attach_selector = 'table#areas > tbody tr#srow-' + rows;
                    //Drupal.attachBehaviors($(attach_selector + ' .unit-convert, ' + attach_selector + ' .units'));
                  }
                  else {
                    alert('Both length and height must be numbers to calculate rectangular area.');
                  }
                  break;

                case 'area-circle':
                var r = $('input#area-circle-radius').val();
                var deg = $('select#area-circle-angle').val();
                var rows = $('.subtotal-row').length;
                var img;
                if ($.isNumeric(r)) {
                  switch (deg) {
                    case 'Full':
                      area = r * r * 3.1415;
                      img = 'area-circle.jpg';
                      break;
                    case 'Half':
                      area = r * r * 3.1415 / 2;
                      img = 'area-halfcircle.jpg';
                      break;
                    case 'Qtr':
                      area = r * r * 3.1415 / 4;
                      img = 'area-quartercircle.jpg';
                      break;
                  }
                  added_area_content = '<tr class="subtotal-row" id="srow-' + rows + '"><td class="medium" style="width:30px;white-space:nowrap;"><img src="/modules/custom/calculator/images/' + img + '" width="30" height="30" border="0">&nbsp;</td><td class="medium" style="width:110px;"><span class="unit-convert">' + r + '</span> <span class="units">' + units + '</span></td><td class="medium area-subtotal" align="right" style="width:110px;"><span class="unit-convert">' + area.toFixed(2) + '</span> <span class="units">sq ' + units + '</span></td></tr>'
                  $('table#areas > tbody .total-row.spacer').before(added_area_content);
                  $('.total-row span.units').html('sq ' + units);
                  $('#area-calculator td.dataheader span#added-areas').show();
                  $('#area-calculator .total-row').show();
                  $('#area-calculator input#clear-areas').show();
                  var attach_selector = 'table#areas > tbody tr#srow-' + rows;
                  //Drupal.attachBehaviors($(attach_selector + ' .unit-convert, ' + attach_selector + ' .units'));
                }
                else {
                  alert('Both length and height must be numbers to calculate triangular area.');
                }
                  break;

                case 'area-triangle':
                var l = $('input#area-triangle-width').val();
                var h = $('input#area-triangle-height').val();
                var rows = $('.subtotal-row').length;
                if ($.isNumeric(l) && $.isNumeric(h)) {
                  area = l * h / 2;
                  added_area_content = '<tr class="subtotal-row" id="srow-' + rows + '"><td class="medium" style="width:30px;white-space:nowrap;"><img src="/modules/custom/calculator/images/area-triangle.jpg" width="30" height="30" border="0">&nbsp;</td><td class="medium" style="width:110px;"><span class="unit-convert">' + l + '</span> <span class="units">' + units + '</span> x <span class="unit-convert">' + h + '</span> <span class="units">' + units + '</span></td><td class="medium area-subtotal" align="right" style="width:110px;"><span class="unit-convert">' + area.toFixed(2) + '</span> <span class="units">sq ' + units + '</span></td></tr>'
                  $('table#areas > tbody .total-row.spacer').before(added_area_content);
                  $('.total-row span.units').html('sq ' + units);
                  $('#area-calculator td.dataheader span#added-areas').show();
                  $('#area-calculator .total-row').show();
                  $('#area-calculator input#clear-areas').show();
                  var attach_selector = 'table#areas > tbody tr#srow-' + rows;
                  //Drupal.attachBehaviors($(attach_selector + ' .unit-convert, ' + attach_selector + ' .units'));
                }
                else {
                  alert('Both length and height must be numbers to calculate triangular area.');
                }
                break;
              }
              $('.area-subtotal span.unit-convert').each(function() {
                area_total += parseFloat($(this).html());
              });
              $('.total-row span.unit-convert').html(area_total.toFixed(2));
              if (units == 'ft') {
                $('#edit-4').val(area_total.toFixed(2));
                $('#edit-4-metric').val((area_total * 0.3048 * 0.3048).toFixed(2));
              }
              else {
                $('#edit-4-metric').val(area_total.toFixed(2));
                $('#edit-4').val((area_total / 0.3048 / 0.3048).toFixed(2));
              }
            });
            break;

          case 'clear-areas':
            $('.subtotal-row').remove();
            $('#area-calculator td.dataheader span#added-areas').hide();
            $('#area-calculator .total-row').hide();
            $('#area-calculator input#clear-areas').hide();
            break;

          case 'calculate-area-close':
            $('table#area_calculator').remove();
            break;

          default:
            break;
        };
      });

    },
    detach: function (context) {}
  };

})(jQuery, Drupal);
