<?php

namespace Drupal\calculator\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Session\Session;



class CalculatorController extends ControllerBase {
  public function saved_reports() {

    $headers = ['Area Name', 'Planting Scheme', 'Square Footage', 'Edit'];

    $query = \Drupal::entityQuery('node');

    $group = $query->orConditionGroup()->
              condition('field_user', \Drupal::currentUser()->id())->
              condition('field_session_id', $_COOKIE['calculator']);

    $nids = $query->condition('type','saved_reports')->
              condition($group)->
              sort('title')->
              execute();

    $nodes =  \Drupal\node\Entity\Node::loadMultiple($nids);


    foreach ($nodes as $n) {

      $url = \Drupal\Core\Url::fromUri('http://rbcalc.techsharpconsulting.com/calculator/1?save_id=' . $n->id());
      $link = \Drupal::l('Edit', $url);

      $rows[] = [ $n->get('title')->value,
                  $n->get('field_calculator_title')->value,
                  $n->get('field_square_feet')->value . 'sqft.',
                  $link,
                ];
    }

    $form = '<form action="/select-planting-scheme" method="GET">
                <input type="button" value="Add new Area" onclick="jQuery(this).hide(); jQuery(\'#add-area\').show();" />
                <div id="add-area" style="display:none;">
                  Area Name:
                  <input type="text" name="area" />
                  <input type="submit" value="Continue &#187;" />
                </div>
              </form>';

    $build = [  [ '#theme' => 'table',
                '#header' => $headers,
                '#rows' => $rows,
              ],
              [ '#type' => 'inline_template',
                '#template' => '{{ somecontent|raw }}',
                '#context' => [
                  'somecontent' => $form,
                ],
              ]
          ];

    $build['#cache'] = ['max-age' => 0,];


    return $build;
  }
}
