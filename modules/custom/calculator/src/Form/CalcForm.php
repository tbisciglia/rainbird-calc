<?php

namespace Drupal\calculator\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\image\Entity\ImageStyle;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class CalcForm.
 */
class CalcForm extends FormBase {

  const SUBMIT_TEXT = 'Calculate Recommendations';
  const RESUBMIT_TEXT = 'Re-calculate Recommendations';
  const SAVE_RESULTS_TEXT = 'Save Result Set';
  const RESET_TEXT = 'Reset';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'calc_form1';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $num = 1) {

    if (isset($_GET['save_id'])) {
      $data =  \Drupal\node\Entity\Node::load($_GET['save_id']);
      $form_data = json_decode($data->get('field_form_data')->value);
      $form_data = get_object_vars($form_data);
    }

    $entityTypeManager = \Drupal::entityTypeManager();
    $storage = [];
    $storage['node'] = $entityTypeManager->getStorage('node');
    $storage['taxo'] = $entityTypeManager->getStorage('taxonomy_term');
    $storage['form'] = $form_state->getStorage();
    $please_select = ['' => $this->t('Please select')];
    $qv = $_GET;

    switch ($num) {
      case 1:
        $title = 'Ground Cover / Dripline Calculator';
        break;

      case 2:
        $title = 'Sparse / Point Source Emitter Calculator';
        break;

      default:
        $title = 'Drip Zone Calculator';
        break;

    }

    switch ($num) {
      case 1:
        $field_calculator = 10;
        $intro = '<p style="text-align: center;">The objective of a well-designed dripline system is to create an even wetted pattern of water throughout the entire area. Different factors affect this.</p>';
        break;

      case 2:
        $field_calculator = 11;
        $intro = '<p>Welcome to the Rain Bird online Point Source Emitter Calculator. '
        . 'By simply answering a few questions, this tool will provide you with the correct '
        . 'product necessary to install your next point source irrigation project.</p>';
        break;
    }

    $question_query = \Drupal::entityQuery('node');
    $question_query->condition('type', 'question')
      ->condition('field_calculator', $field_calculator)
      ->sort('created', 'ASC');
    $question_nids = $question_query->execute();
    $question = $storage['node']->loadMultiple($question_nids);

    $form = [];

    $form['#title'] = $title;

    if (isset($form_data['calculator_title'])) {
      $form['calculator_title'] = array('#type' => 'hidden', '#value' => $form_data['calculator_title']);
    } else {
      $form['calculator_title'] = array('#type' => 'hidden', '#value' => $title);
    }

    if (isset($form_data['field_calculator'])) {
      $form['field_calculator'] = array('#type' => 'hidden', '#value' => $form_data['field_calculator']);
    } else {
      $form['field_calculator'] = array('#type' => 'hidden', '#value' => $field_calculator);
    }



    if (isset($_GET['save_id'])) {
      $form['save_id'] = array('#type' => 'hidden', '#value' => $_GET['save_id']);
    }

    $form['#attached'] = [
      'library' => ['calculator/calculator'],
    ];

    $form['intro'] = [
      '#markup' => $this->t($intro),
    ];

    if ($num == 1) {
      $form['units'] = [
        '#type' => 'select',
        '#title' => 'Before you begin, please choose the desired units of measurement.',
        '#options' => ['English' => 'English', 'Metric' => 'Metric'],
        '#size' => 1,
        '#default_value' => isset($qv['units']) ? $qv['units'] : 'English',
        '#required' => TRUE,
        '#suffix' => '<h2>Questions</h2>',
      ];
    }

    $q_num = 0;
    $default_answers = [14];
    foreach ($question as $q) {
      $answer_query = \Drupal::entityQuery('node');
      $answer_query->condition('type', 'question_options')
        ->condition('field_question', $q->id());
      $answer_nids = $answer_query->execute();
      $answer = $storage['node']->loadMultiple($answer_nids);
      $options = [];
      $field_type = $q->field_type->value;
      $default_value = NULL;
      foreach ($answer as $a) {
        $options[$a->id()] = $a->field_answer_text->value;
        if (isset($qv[$q->id()])) {
          $default_value = $qv[$q->id()];
        }
        elseif (isset($form_data[$q->id()])) {
          $default_value = $form_data[$q->id()];
        }
        elseif (in_array($a->id(), $default_answers)) {
          $default_value = $a->id();
        }
      }

      $image = null;
      if (isset($q->field_rollover_image->entity)) {
        $image = "<img src='" . file_create_url($q->field_rollover_image->entity->getFileUri()) . "' />";
      }

      switch ($field_type) {
        case 'radios':
        case 'checkboxes':
        case 'select':
          $form[$q->id()] = [
            '#type' => $field_type,
            '#title' => ++$q_num . '. ' . $q->getTitle() . '<div class="question__hover">' . $image . $q->get('body')->value  . '</div>',
            '#required' => (boolean) $q->field_required->value,
            '#options' => $options,
            '#default_value' => $default_value,
          ];
          break;

        case 'textfield':
          $default_value = isset($qv[$q->id()]) ? $qv[$q->id()] : '';

          if (isset($qv[$q->id()])) {
            $default_value = $qv[$q->id()];
          } elseif (isset($form_data[$q->id()])) {
            $default_value = $form_data[$q->id()];
          }

          $size = 40;
        case 'number':
          if (is_null($default_value)) {
            $default_value = 0;

            if (isset($qv[$q->id()])) {
              $default_value = $qv[$q->id()];
            } elseif (isset($form_data[$q->id()])) {
              $default_value = $form_data[$q->id()];
            }

            $size = 6;
          }
          if (count($answer_nids) > 0) {
            $form[$q->id()] = [
              '#markup' => '<div>' . ++$q_num . '. ' . $q->getTitle() . '</div>',
            ];
            foreach ($answer as $a) {
              $default_value = isset($qv[$q->id() . '_' . $a->id()]) ? $qv[$q->id() . '_' . $a->id()] : $default_value;

              if (isset($qv[$q->id() . '_' . $a->id()])) {
                $default_value = $qv[$q->id()  . '_' . $a->id()];
              } elseif (isset($form_data[$q->id()  . '_' . $a->id()])) {
                $default_value = $form_data[$q->id() . '_' . $a->id()];
              }

              $form[$q->id() . '_' . $a->id()] = [
                '#type' => $field_type,
                '#title' => $a->field_answer_text->value . '<div class="question__hover">' . $image . $q->get('body')->value  . '</div>',
                '#default_value' => $default_value,
                '#size' => $size,
              ];
            }
          }
          else {
            $form[$q->id()] = [
              '#type' => $field_type,
              '#title' => ++$q_num . '. ' . $q->getTitle() . '<div class="question__hover">' . $image . $q->get('body')->value  . '</div>',
              '#size' => $size,
            ];
          }
      }


      if ($q->id() == 4) {

        $default_value = '';

        if (isset($qv[$q->id()])) {
          $default_value = $qv[$q->id()];
        } elseif (isset($form_data[$q->id()])) {
          $default_value = $form_data[$q->id()];
        }

        $form[$q->id()]['#field_suffix'] = 'sq ft (<a href="#" class="sf-calc">calculate</a>)';
        $form[$q->id()]['#attributes']['class'][] = 'field_english';
        $form[$q->id()]['#default_value'] = $default_value;

        $form[$q->id() . '_metric'] = [
          '#type' => $field_type,
          '#title' => $q_num . '. ' . $q->getTitle() . '<div class="question__hover">' . $image . $q->get('body')->value  . '</div>',
          '#size' => 6,
          '#field_suffix' => 'sq meters (<a href="#" class="sf-calc">calculate</a>)',
          '#attributes' => ['class' => ['field_metric']],
          '#suffix' => '<div id="area-calculator"></div>'
        ];
      }

      if ($q->id() == 6) {
        $form[$q->id()]['#title'] = --$q_num . 'B. ' . $q->getTitle();
      }

    }

    $form['num'] = [
      '#type' => 'hidden',
      '#value' => $num,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => CalcForm::SUBMIT_TEXT,
    ];
    $form['reset'] = [
      '#type' => 'submit',
      '#value' => CalcForm::RESET_TEXT,
    ];

    if (isset($storage['form']['results'])) {
      $form['save_name'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Save these results as:'),
        '#description' => '<br>' . $this->t('Once you have the results you\'re looking for, you can temporarily save them and continue calculating other areas of your landscape.'),
        '#disabled' => TRUE,
        '#value' => $_GET['area'],
      ];
      $form['save_results'] = [
        '#type' => 'submit',
        '#value' => CalcForm::SAVE_RESULTS_TEXT,
      ];
      $form['results'] = $storage['form']['results'];
      $form['submit']['#value'] = CalcForm::RESUBMIT_TEXT;
    }

    if (isset($form_data)) {
      $results = $this->generateResults($form_data);

      $form['save_name'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Save these results as:'),
        '#description' => '<br>' . $this->t('Once you have the results you\'re looking for, you can temporarily save them and continue calculating other areas of your landscape.'),
        '#disabled' => TRUE,
        '#value' => $data->get('title')->value,
      ];
      $form['save_results'] = [
        '#type' => 'submit',
        '#value' => CalcForm::SAVE_RESULTS_TEXT,
      ];
      $form['results'] = $results;
      $form['submit']['#value'] = CalcForm::RESUBMIT_TEXT;

    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $num = $form_state->getValue('num');

    switch ($num) {
      case 1:
        $num_eng = $form_state->getValue('4');
        $num_met = $form_state->getValue('4_metric');
        if (!is_numeric($num_eng)) {
          $field_error = ['4'];
          $form_state->setError($field_error, 'Area to be irrigated (English) not a valid number. Please re-enter.');
        }
        if (!is_numeric($num_met)) {
          $field_error = ['4_metric'];
            $form_state->setError($field_error, 'Area to be irrigated (Metric) not a valid number. Please re-enter.');
        }
        break;
    }

    parent::validateForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $values = $form_state->getValues();
    if (in_array($values['op'], [CalcForm::SUBMIT_TEXT, CalcForm::RESUBMIT_TEXT])) {
      $results = $this->generateResults($values);
      $form_state->setStorage([
        'results' => $results,
      ]);
      $form_state->setRebuild(TRUE);
    }
    elseif ($values['op'] == CalcForm::SAVE_RESULTS_TEXT) {

      if (!isset($_SESSION['calc'])) {
        $_SESSION['calc'] = [];
      }
      $_SESSION['calc'][] = $values;

      $ts = $_COOKIE['calculator'];

      if (!isset($ts) || ($ts == '')) {
        $ts = time();
        setcookie('calculator', $ts, time() + 3600, '/');
      }

      if (isset($values['save_id'])) {
        $node =  \Drupal\node\Entity\Node::load($values['save_id']);

        $node->set('title', $values['save_name']);

        $node->set('field_form_data', json_encode($values));

        if (\Drupal::currentUser()->id() != 0) {
          $node->set('field_user', \Drupal::currentUser()->id());
        }

        $node->set('field_square_feet', $values['4']);

        $node->set('field_calculator_title', $values['calculator_title']);

        $node->set('field_calculator_term', $values['field_calculator']);

        $node->set('field_session_id', $ts);

        $node->save();

      } else {
        $node = entity_create('node', ['type' => 'saved_reports']);

        $node->set('title', $values['save_name']);

        $node->set('field_form_data', json_encode($values));

        if (\Drupal::currentUser()->id() != 0) {
          $node->set('field_user', \Drupal::currentUser()->id());
        }

        $node->set('field_square_feet', $values['4']);

        $node->set('field_calculator_title', $values['calculator_title']);

        $node->set('field_calculator_term', $values['field_calculator']);

        $node->set('field_session_id', $ts);

        $node->save();
      }

      drupal_set_message('Your calculations have been saved.');

      $response = new RedirectResponse(\Drupal::url('<front>'));
      $response->send();
    }
    elseif ($values['op'] == CalcForm::RESET_TEXT) {
      $form_state->setRebuild(FALSE);
    }

  }

/**
   * Clears form input.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */

  protected function clearFormInput(array $form, FormStateInterface $form_state) {
    // Clear user input.
    $input = $form_state->getUserInput();
    // We should not clear the system items from the user input.
    $clean_keys = $form_state->getCleanValueKeys();
    $clean_keys[] = 'ajax_page_state';
    foreach ($input as $key => $item) {
      if (!in_array($key, $clean_keys) && substr($key, 0, 1) !== '_') {
        unset($input[$key]);
      }
    }
    $form_state->setUserInput($input);
    // Rebuild the form state values.
    $form_state->setRebuild();
    $form_state->setStorage([]);
  }

  protected function generateResults($values) {

    $entityTypeManager = \Drupal::entityTypeManager();
    $storage = [];
    $storage['node'] = $entityTypeManager->getStorage('node');

    $output = [];
    $output['separator'] = [
      '#markup' => '<hr class="results-separator">',
    ];
    $units = $values['units'];

    switch($values['num']) {

      case 1:

        $dripline_rows = [];
        $area_answer = $storage['node']->load($values['1']);
        $soil_answer = $storage['node']->load($values['2']);
        $sloped_answer = $storage['node']->load($values['3']);
        $rescom_answer = $storage['node']->load($values['5']);
        $reclaim_answer = $storage['node']->load($values['6']);
        $area_details = empty($area_answer) ? [] : $area_answer->field_result_info->getValue();
        $soil_details = empty($soil_answer) ? [] : $soil_answer->field_result_info->getValue();
        $rescom_details = empty($rescom_answer) ? [] : $rescom_answer->field_result_info->getValue();
        $reclaim_details = empty($reclaim_answer) ? [] : $reclaim_answer->field_result_info->getValue();
        $sloped_details = empty($sloped_answer) ? [] : $sloped_answer->field_result_info->getValue();

        // if slope is over 10 feet, show content page instead of results table

        if (isset($values[3]) && $values[3] == 13) {
          $referral_nid = $sloped_details[0]['value'];
          if (!empty($referral_nid)) {
            $referral_content = $storage['node']->load($referral_nid);
            $output['referral'] = [
              '#markup' => '<h2>' . $referral_content->getTitle() . '</h2><div>' . $referral_content->body->value . '</div>',
            ];
          }
        }

        else {

          $part_type = $area_details[0]['value'];
          $flow_rate_text = $soil_details[0]['value'];
          $flow_rate = (int) $flow_rate_text;
          $spacing_text = $area_details[1]['value'];
          //\Drupal::logger('calcform')->notice('spacing_text: <pre>%val</pre>', array('%val' => $spacing_text));
          $spacing = (int) $spacing_text;
          if (isset($values['4']) && !empty($values['4'])) {
            $area = $values['4'];
            $area_eng = $area_val = $this->getConversion('English', (int) $area, 'ft');
          }
          elseif (isset($values['4_metric']) && !empty($values['4_metric'])) {
            $area = $values['4_metric'];
            $area_eng = $this->getConversion('English', (int) $area, 'm');
            $area_val = $this->getConversion('Metric', (int) $area, 'm');
          }
          else {
            $area_val = $this->getConversion($units, 0, 'ft');
          }

          $dripline_part = $part_type . '-' . $flow_rate_text. '-' . $spacing_text;

          $dripline_val = $this->getConversion($units, $area_val->value * 12 / $spacing, 'ft');
          $dripline_eng = $this->getConversion('English', $area_val->value * 12 / $spacing, 'ft');
          $spacing_val = $this->getConversion($units, $spacing, 'in');

          $flow_rate_val = $this->getConversion($units, $flow_rate, 'GPH');

          $gpm = (float) $area_eng->value * $flow_rate * 2.4 / $spacing / $spacing / 10;
          $gpm_val = $this->getConversion($units, $gpm, 'GPM');

          $stake_part = 'TDS050';
          $stake_product = $this->getProduct($stake_part);
          $stake_url = $this->getProductUrl($stake_product, 'external', 'part');
          $stake_pic_url = ImageStyle::load('thumbnail')->buildUrl($stake_product->field_image->entity->getFileUri());
          $stake_pic_markup = [
            '#markup' => '<img src="' . file_url_transform_relative($stake_pic_url) . '" border="0">',
          ];
          $stake_pic = drupal_render($stake_pic_markup);

          $stake_num = ceil($dripline_eng->value / $soil_details[1]['value']);

          $flow = [
            '#markup' => '<span class="unit-convert">' . $flow_rate_val->value_format . '</span> <span class="unit">' . $flow_rate_val->units . '</span>',
          ];
          $spacing_text = [
            '#markup' => '<span class="unit-convert">' . $spacing_val->value_format . '</span> <span class="unit">' . $spacing_val->units . '</span>',
          ];
          $length = [
            '#markup' => '<span class="unit-convert">' . ceil($dripline_val->value) . '</span> <span class="unit">' . $dripline_val->units . '</span>',
          ];
          $gpm_text = [
            //'#markup' => '<span class="unit-convert">' . $gpm_val->value_format . '</span> <span class="unit">' . $gpm_val->units . '</span>',
            '#markup' => '<span class="gpm">' . number_format($gpm, 2) . '</span> GPM',
          ];

          $max_gpm = [
            '#type' => 'select',
            //'#title' => 'Max ' . $gpm_val->units,
            '#options' => [
              20 => 20,
              40 => 40,
            ],
            '#attributes' => ['class' => ['max-gpm']],
          ];

          $num_kits = ceil($gpm / 20);

          $step = 0;
          $czk = 'No matching kits exist';
          if ($values['5'] == 16 && isset($values['6']) && $values['6'] == 17) {
            if (isset($reclaim_details[0]['value']) && !empty($reclaim_details[0]['value'])) {
              $czk = $reclaim_details[0]['value'];
            }
          }
          else {
            while (isset($rescom_details[$step]['value']) && !empty($rescom_details[$step]['value'])) {
              if (
                (
                  isset($rescom_details[$step + 1]['value']) &&
                  is_numeric($rescom_details[$step + 1]['value']) &&
                  $gpm <= $rescom_details[$step + 1]['value']
                ) ||
                (
                  !isset($rescom_details[$step + 1]['value']) ||
                  empty($rescom_details[$step + 1]['value'])
                )
              ) {
                $czk = $rescom_details[$step]['value'];
                break;
              }
              else {
                $step += 2;
              }
            }
          }
          $czk_url = $this->getProductUrl($czk, 'external', 'part');

          $dripline_rows[] = [
            'type' => 'Dripline ' . $part_type,
            'part_number' => $dripline_part,
            'length' => drupal_render($length),
            'flow' => drupal_render($flow),
            'spacing' => drupal_render($spacing_text),
          ];
          $dripline_table = [
            '#type' => 'table',
            '#header' => ['Type', 'Part Number', 'Dripline Length Needed', 'Emitter Flow', 'Emitter Spacing'],
            '#rows' => $dripline_rows,
          ];

          $gpm_header = [
            '#markup' => 'GPM', //'<span class="unit">' . $gpm_val->units . '</span>',
          ];


          $czk_table = [
            '#type' => 'table',
            '#header' => [
              drupal_render($gpm_header),
              'Control Zone Kit',
              'Max GPM',// . $gpm_val->units,
              'Number of Kits'],
            '#rows' => [
              [
                'gpm' => drupal_render($gpm_text),
                'czk' => $czk_url,
                'max_gpm' => drupal_render($max_gpm),
                'num_kits' => [
                  'data' => $num_kits,
                  'class' => 'num-kits',
                ],
              ],
            ]
          ];

          $tools_quantity = ceil($dripline_eng->value / 5000);

          $tools_table = [
            '#type' => 'table',
            '#header' => [
              'Cutter',
              'Flush Valves',
              'Fittings Tool',
            ],
            '#rows' => [
              [
                $tools_quantity,
                $tools_quantity,
                $tools_quantity,
              ],
            ],
          ];

          $output['debug'] = [
            //'#markup' => '<pre>' . print_r($area_eng, TRUE) . print_r($area_val, TRUE) . '</pre><p>area: ' . (float) $area_eng->value . '; GPM: ' . $gpm . '</p>',
          ];

          /*
          $output['recommendations'] = [
            '#markup' => '<h2>Recommendations</h2><h3>Recommended Drip Tubing</h3>',
          ];
          */

          $output['recommendations'] = [
            '#markup' => '<h3 class="results-header">Thanks! Here\'s what we recommend.</h3>',
          ];

          $output['dripline'] = $dripline_table . '</div> <!-- end .results__item -->';

          $output['stakes_intro'] = [
            '#markup' => '<div class="results__item row1 first col-sm-4">' . '<h3>Stakes</h3>',
          ];

          $stakes_rows = [];
          $stakes_rows[] = [
            'part_number' => $stake_url,
            'stakes' => $stake_num . ' stake' . ($stake_num == 1 ? '' : 's'),
            'pic' => $stake_pic,
          ];
          $stakes_table = [
            '#type' => 'table',
            '#header' => ['Part Number', 'Number of Stakes', ''],
            '#rows' => $stakes_rows,
          ];
          $output['stakes'] = $stakes_table;

          $output['stakes_close'] = ['#markup' => '</div> <!-- end .results__item -->'];

          $output['fittings_intro'] = [
            '#markup' => '<div class="results__item row1 col-sm-4">' . '<h3>Fittings</h3>',
          ];

          $output['fittings'] = [
            '#markup' => '<p>On average, 40% of your total fittings will be Ts, 40% Elbows and 20% Couplings. Please use this as a general guideline when determining your specific configuration.</p>',
          ];

          $output['fittings_close'] = ['#markup' => '</div> <!-- end .results__item -->'];

          $output['czk_intro'] = [
            '#markup' => '<div class="results__item row1 col-sm-4">' . '<h3>Control Zone Kit</h3>',
          ];

          $output['czk_table'] = $czk_table;

          $output['czk_close'] = ['#markup' => '</div> <!-- end .results__item -->'];

          $output['tools_intro'] = [
            '#markup' => '<div class="results__item first row2 col-sm-4">' . '<h3>Tools</h3>',
          ];

          $output['tools_table'] = $tools_table;

          $output['tools_close'] = ['#markup' => '</div> <!-- end .results__item -->'];

          $output['operational_indicator'] = [
            '#markup' => '<div class="results__item row2 col-sm-4">'  . '<p>Operation Indicator: <span class="num-kits">' . $num_kits . '</span></p>' . '</div> <!-- end .results__item -->',
          ];

          $output['filter'] = [
            '#markup' => '<div class="results__item row2 col-sm-4"></div> <!-- end .results__item -->',
          ];
        }
        break;

      case 2:

        $canopy_query = \Drupal::entityQuery('node');
        $canopy_query->condition('type', 'question_options')
          ->condition('field_question', 22);
        $canopy_nids = $canopy_query->execute();
        $canopy_answers = $storage['node']->loadMultiple($canopy_nids);

        $soil_answer = $storage['node']->load($values['2']);
        $reclaim_answer = $storage['node']->load($values['6']);
        $dist_answer = $storage['node']->load($values['23']);
        $soil_details = empty($soil_answer) ? [] : $soil_answer->field_result_info->getValue();
        $reclaim_details = empty($reclaim_answer) ? [] : $reclaim_answer->field_result_info->getValue();
        $dist_details = empty($dist_answer) ? [] : $dist_answer->field_result_info->getValue();

        $rows = [];

        $emitter_option_1 = isset($soil_details[2]['value']) ? $soil_details[2]['value'] : 'invalid';
        $emitter_option_2 = isset($soil_details[3]['value']) ? $soil_details[3]['value'] : 'invalid';
        $emitter_classes = ['emitter-gpm', ''];
        //kint($dist_details);

        if (count($dist_details) > 1) {
          $emitter_type_options = [];
          foreach ($dist_details as $dist) {
            $emitter_type_options[$dist['value']] = $dist['value'];
          }
          $emitter_type = [
            '#type' => 'select',
            '#options' => $emitter_type_options,
          ];
        }
        else {
          $emitter_type = [
            '#markup' => $dist_details[0]['value'],
          ];
        }
        //kint($emitter_type);
        $emitter_type_markup = drupal_render($emitter_type);
        //kint($emitter_type);
        //kint($emitter_type_markup);

        foreach ($canopy_answers as $ca) {
          $emitter_classes[1] = 'emitter-gpm-' . $ca->id();
          $emitter_choice = [
            '#type' => 'select',
            '#options' => [
              '' => '- Select one -',
              $emitter_option_1 => $emitter_option_1,
              $emitter_option_2 => $emitter_option_2,
            ],
            '#attributes' => ['class' => $emitter_classes],
          ];
          $emitter_choice['#attributes']['class'] = $emitter_classes;
          $num_canopies = (int) $values['22_' . $ca->id()];
          $quantity_text = [
            '#markup' => '<b>' . $num_canopies . '</b> (' . $ca->field_answer_text->value . ')',
          ];
          $canopy_value = (float) ($ca->field_result_info->getValue()[0]['value']);
          $emitter_min = (float) ($ca->field_result_info->getValue()[1]['value']);
          $soil_value = (float) ($soil_details[4]['value']);

          $emitter_value = ceil($num_canopies * $canopy_value * $canopy_value * 0.7854 / $soil_value);

          $row = [
            drupal_render($quantity_text),
            drupal_render($emitter_choice),
            max($emitter_value, $emitter_min),
            $emitter_type_markup,
          ];

          $rows[] = $row;
        }

        $result_table_header = [
          'Plant Quantity',
          'Emitter Flow Rate/GPH',
          'Total Emitters',
          'Emitter Type',
        ];
        $result_table = [
          '#type' => 'table',
          '#header' => $result_table_header,
          '#rows' => $rows,
        ];

        $output['result_table'] = $result_table;

        return $output;

        break;
    }

    return $output;
  }

  protected function getConversion($system, $value, $units) {
    $class_value = 'value-' . strtolower($system);
    $class_units = 'units-' . strtolower($system);
    if ($system == 'Metric') {
      switch ($units) {
        case 'in':
          $units = 'cm';
          $value = $value * 2.54;
          break;
        case 'in/hr':
          $units = 'mm/hr';
          $value = $value * 25.4;
          break;
        case 'ft':
          $units = 'm';
          $value = $value * 0.3048;
          break;
        case 'GPH':
          $units = 'LPH';
          $value = $value * 3.78541;
          break;
        case 'GPM':
          $units = 'LPM';
          $value = $value * 3.78541;
          break;
        case 'psi':
          $units = 'bars';
          $value = $value * 0.0689476;
          break;
        case 'cm':
        case 'mm/hr':
        case 'm':
        case 'LPH':
        case 'LPM':
        case 'bars':
          $value = $value;
          break;
        default:
          $units = 'UNKNOWN';
          $value = 1;
      }
    }
    elseif ($system == 'English') {
      switch ($units) {
        case 'cm':
          $units = 'in';
          $value = $value / 2.54;
          break;
        case 'mm/hr':
          $units = 'in/hr';
          $value = $value / 25.4;
          break;
        case 'm':
          $units = 'ft';
          $value = $value / 0.3048;
          break;
        case 'LPH':
          $units = 'GPH';
          $value = $value / 3.78541;
          break;
        case 'LPM':
          $units = 'GPM';
          $value = $value / 3.78541;
          break;
        case 'bars':
          $units = 'psi';
          $value = $value / 0.0689476;
          break;
        case 'in':
        case 'in/hr':
        case 'ft':
        case 'GPH':
        case 'GPM':
        case 'psi':
          break;
        default:
          $units = 'UNKNOWN';
          $value = 1;
      }
    }
    $result = new \stdClass();
    $result->value = $value;
    $result->value_format = number_format($value, 2);
    $result->units = $units;
    $result->class_value = $class_value;
    $result->class_units = $class_units;
    return $result;
  }

  protected function getProduct($part_num) {
    $result = NULL;
    if (!empty($part_num)) {
      $entityTypeManager = \Drupal::entityTypeManager();
      $storage = $entityTypeManager->getStorage('node');
      $product_query = \Drupal::entityQuery('node');
      $product_query->condition('type', 'product')
        ->condition('status', 1)
        ->condition('field_part_number', $part_num, 'LIKE')
        ->sort('created', 'ASC');
      $product_nids = $product_query->execute();
      $product_count = count($product_nids);
      if ($product_count > 1) {
        \Drupal::logger('calcform')->notice('Multiple published products matching part number %pn: id numbers are %ids', ['%pn' => $part_num, '%ids' => implode(', ', $product_nids)]);
      }
      if ($product_count > 0) {
        $id = array_pop($product_nids);
        $result = $storage->load($id);
      }
    }
    return $result;
  }

  protected function getProductUrl($obj, $url_type, $link_text) {

    // $url_type is 'external' if you want to retrieve the external URL (stored in field_product_url), or 'node' if you want the node URL.

    $type = gettype($obj);
    \Drupal::logger('calcform')->notice('type is %type', ['%type' => $type]);
    if ($type == 'string') {
      $product = $this->getProduct($obj);
    }
    elseif ($type == 'object' && get_class($obj)) {
      $product = $obj;
    }
    else {
      $product = NULL;
    }
    \Drupal::logger('calcform')->notice('type is %type', ['%type' => $type]);

    if (is_null($product)) {
      return NULL;
    }
    else {
      switch ($link_text) {
        case 'title':
          $link_text = $product->getTitle();
          break;

        case 'part':
        case 'part_number':
        case 'part number':
          $link_text = $product->field_part_number->value;
          break;

        default:
          // Do nothing; this way, the link text will literally be what was passed.
          break;
      }
      if (empty($link_text)) {
        $link_text = 'Product';
      }

      switch ($url_type) {
        case 'node':
          // Use below if you want the URL for the node itself.
          $url = Url::fromRoute('entity.node.canonical', ['node' => $product->id()]);
          if (empty($url)) {
            $return = $product->field_part_number->value;
          }
          else {
            $markup = ['#markup' => Link::fromTextAndUrl($link_text, $url)->toString()];
            $return = drupal_render($markup);
          }
          break;

        case 'external':
        case 'product':
        default:
          // Use below if you want the URL from the product_url field.
          $url = $product->field_product_url->uri;
          if (!empty($url)) {
            $markup = ['#markup' => '<a href=' . $url . ' target="_blank">' . $link_text . '</a>'];
            $result = drupal_render($markup);
          }
          break;
      }
    }

    return $result;
  }
}