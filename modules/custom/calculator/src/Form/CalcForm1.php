<?php

namespace Drupal\calculator\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Core\Ajax\AjaxResponse;

/**
 * Class CalcForm1.
 */
class CalcForm1 extends FormBase {

  const SUBMIT_TEXT = 'Calculate Recommendations';
  const RESET_TEXT = 'Reset';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'calc_form1';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $entityTypeManager = \Drupal::entityTypeManager();
    $storage = [];
    $storage['node'] = $entityTypeManager->getStorage('node');
    $storage['taxo'] = $entityTypeManager->getStorage('taxonomy_term');
    $storage['form'] = $form_state->getStorage();
    $please_select = ['' => $this->t('Please select')];

    $question_query = \Drupal::entityQuery('node');
    $question_query->condition('type', 'question')
      ->condition('field_calculator', 10);
    $question_nids = $question_query->execute();
    $question = $storage['node']->loadMultiple($question_nids);

/*
    $area_type_terms = $storage['taxo']->loadTree('area_type');
    $area_types = $please_select;
    foreach ($area_type_terms as $term) {
      $area_types[$term->tid] = $term->name;
    }

    $soil_type_vocab = $storage['taxo']->loadTree('soil_type');
    $soil_types = $please_select;
    foreach ($soil_type_vocab as $term) {
      $soil_types[$term->tid] = $term->name;
    }
*/

    $form['#attached'] = [
      'library' => ['calculator/calculator'],
    ];
    $form['intro'] = [
//      '#markup' => $this->t('<p>Welcome to the Rain Bird online Landscape Drip Zone Calculator. '
//        . 'By simply answering a few questions, this tool will provide you with the correct '
//        . 'product necessary to install your next dripline irrigation project.</p>'),

      '#markup' => $this->t('<p style="text-align: center;">The objective of a well-designed dripline system is to create an even wetted pattern of water throughout the entire area. Different factors affect this.</p>'),
    ];
    $form['units'] = [
      '#type' => 'select',
      '#title' => 'Before you begin, please choose the desired units of measurement.',
      '#options' => ['English' => 'English', 'Metric' => 'Metric'],
      '#size' => 1,
      '#default_value' => 'English',
      '#required' => TRUE,
      '#suffix' => '<h2>Questions</h2>',
    ];

    $q_num = 0;
    foreach ($question as $q) {
      $answer_query = \Drupal::entityQuery('node');
      $answer_query->condition('type', 'question_options')
        ->condition('field_question', $q->id());
      $answer_nids = $answer_query->execute();
      if (count($answer_nids) > 0) {
        $answer = $storage['node']->loadMultiple($answer_nids);
        $options = [];
        foreach ($answer as $a) {
          $options[$a->id()] = $a->field_answer_text->value;
        }
        $form[$q->id()] = [
          '#type' => 'radios',
          '#title' => ++$q_num . '. ' . $q->getTitle(),
          '#options' => $options,
          '#required' => TRUE,
        ];
        if ($q->id() == 6) {
          $form[$q->id()]['#required'] = FALSE;
        }
      }
      else {
        $form[$q->id()] = [
          '#type' => 'textfield',
          '#title' => ++$q_num . '. ' . $q->getTitle(),
          '#size' => 6,
        ];
        if ($q->id() == 4) {
          $form[$q->id()]['#field_suffix'] = 'sq ft';
          $form[$q->id()]['#attributes']['class'][] = 'field_english';
          $form[$q->id() . '_metric'] = [
            '#type' => 'textfield',
            '#title' => $q_num . '. ' . $q->getTitle(),
            '#size' => 6,
            '#field_suffix' => 'sq meters',
            '#attributes' => ['class' => ['field_metric']],
          ];
        }
      }
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => CalcForm1::SUBMIT_TEXT,
    ];
    $form['reset'] = [
      '#type' => 'submit',
      '#value' => CalcForm1::RESET_TEXT,
    ];
    $form['results'] = isset($storage['form']['results']) ? $storage['form']['results'] : NULL;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $num_eng = $form_state->getValue('4');
    $num_met = $form_state->getValue('4_metric');
    if (!is_numeric($num_eng)) {
      $field_error = ['4'];
      $form_state->setError($field_error, 'Area to be irrigated (English) not a valid number. Please re-enter.');
    }
    if (!is_numeric($num_met)) {
      $field_error = ['4_metric'];
      $form_state->setError($field_error, 'Area to be irrigated (Metric) not a valid number. Please re-enter.');
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if ($values['op'] == CalcForm1::SUBMIT_TEXT) {
      $results = $this->generateResults($values);
      $form_state->setStorage([
        'results' => $results,
      ]);
      $form_state->setRebuild(TRUE);
    }
    elseif ($values['op'] == CalcForm1::RESET_TEXT) {
    }
  }

/**
   * Clears form input.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */

  protected function clearFormInput(array $form, FormStateInterface $form_state) {
    // Clear user input.
    $input = $form_state->getUserInput();
    // We should not clear the system items from the user input.
    $clean_keys = $form_state->getCleanValueKeys();
    $clean_keys[] = 'ajax_page_state';
    foreach ($input as $key => $item) {
      if (!in_array($key, $clean_keys) && substr($key, 0, 1) !== '_') {
        unset($input[$key]);
      }
    }
    $form_state->setUserInput($input);
    // Rebuild the form state values.
    $form_state->setRebuild();
    $form_state->setStorage([]);
  }

  protected function generateResults($values) {
    $entityTypeManager = \Drupal::entityTypeManager();
    $storage = [];
    $storage['node'] = $entityTypeManager->getStorage('node');

    $output = [];
    $units = $values['units'];
    $dripline_rows = [];
    $area_answer = $storage['node']->load($values['1']);
    $soil_answer = $storage['node']->load($values['2']);
    $rescom_answer = $storage['node']->load($values['5']);
    $area_details = $area_answer->field_result_info->getValue();
    $soil_details = $soil_answer->field_result_info->getValue();
    $rescom_details = $rescom_answer->field_result_info->getValue();

    $part_type = $area_details[0]['value'];
    $flow_rate_text = $soil_details[0]['value'];
    $flow_rate = (int) $flow_rate_text;
    $spacing_text = $area_details[1]['value'];
    //\Drupal::logger('calcform1')->notice('spacing_text: <pre>%val</pre>', array('%val' => $spacing_text));
    $spacing = (int) $spacing_text;
    if (isset($values['4']) && !empty($values['4'])) {
      $area = $values['4'];
      $area_eng = $area_val = $this->getConversion('English', (int) $area, 'ft');
    }
    elseif (isset($values['4_metric']) && !empty($values['4_metric'])) {
      $area = $values['4_metric'];
      $area_eng = $this->getConversion('English', (int) $area, 'm');
      $area_val = $this->getConversion('Metric', (int) $area, 'm');
    }
    else {
      $area_val = $this->getConversion($units, 0, 'ft');
    }

    $dripline_part = $part_type . '-' . $flow_rate_text. '-' . $spacing_text;

    $dripline_val = $this->getConversion($units, $area_val->value * 12 / $spacing, 'ft');
    $dripline_eng = $this->getConversion('English', $area_val->value * 12 / $spacing, 'ft');
    $spacing_val = $this->getConversion($units, $spacing, 'in');

    $flow_rate_val = $this->getConversion($units, $flow_rate, 'GPH');

    $gpm = (float) $area_eng->value * $flow_rate * 2.4 / $spacing / $spacing / 10;
    $gpm_val = $this->getConversion($units, $gpm, 'GPM');

    $stake_part = 'TDS050';

    $stake_num = ceil($dripline_eng->value / $soil_details[1]['value']);

    $flow = [
      '#markup' => '<span class="unit-convert">' . $flow_rate_val->value_format . '</span> <span class="unit">' . $flow_rate_val->units . '</span>',
    ];
    $spacing_text = [
      '#markup' => '<span class="unit-convert">' . $spacing_val->value_format . '</span> <span class="unit">' . $spacing_val->units . '</span>',
    ];
    $length = [
      '#markup' => '<span class="unit-convert">' . ceil($dripline_val->value) . '</span> <span class="unit">' . $dripline_val->units . '</span>',
    ];
    $gpm_text = [
      //'#markup' => '<span class="unit-convert">' . $gpm_val->value_format . '</span> <span class="unit">' . $gpm_val->units . '</span>',
      '#markup' => '<span class="gpm">' . number_format($gpm, 2) . '</span> GPM',
    ];

    $max_gpm = [
      '#type' => 'select',
      //'#title' => 'Max ' . $gpm_val->units,
      '#options' => [
        20 => 20,
        40 => 40,
      ],
      '#attributes' => ['class' => ['max-gpm']],
    ];

    $num_kits = ceil($gpm / 20);

    $step = 0;
    $czk = 'No matching kits exist';
    if ($values['5'] == 16 && isset($values['6']) && $values['6'] == 17) {
      $czk = 'XCZ-100-PRBR';
    }
    else {
      while (isset($rescom_details[$step]['value']) && !empty($rescom_details[$step]['value'])) {
        if (
          (
            isset($rescom_details[$step + 1]['value']) &&
            is_numeric($rescom_details[$step + 1]['value']) &&
            $gpm <= $rescom_details[$step + 1]['value']
          ) ||
          (
            !isset($rescom_details[$step + 1]['value']) ||
            empty($rescom_details[$step + 1]['value'])
          )
        ) {
          $czk = $rescom_details[$step]['value'];
          break;
        }
        else {
          $step += 2;
        }
      }
    }

    $dripline_rows[] = [
      'type' => 'Dripline ' . $part_type,
      'part_number' => $dripline_part,
      'length' => drupal_render($length),
      'flow' => drupal_render($flow),
      'spacing' => drupal_render($spacing_text),
    ];
    $dripline_table = [
      '#type' => 'table',
      '#header' => ['Type', 'Part Number', 'Dripline Length Needed', 'Emitter Flow', 'Emitter Spacing'],
      '#rows' => $dripline_rows,
    ];

    $czk_table = [
      '#type' => 'table',
      '#header' => [
        drupal_render($gpm_header),
        'Control Zone Kit',
        'Max GPM',// . $gpm_val->units,
        'Number of Kits'],
      '#rows' => [
        [
          'gpm' => drupal_render($gpm_text),
          'czk' => $czk,
          'max_gpm' => drupal_render($max_gpm),
          'num_kits' => [
            'data' => $num_kits,
            'class' => 'num-kits',
          ],
        ],
      ]
    ];

    $tools_quantity = ceil($dripline_eng->value / 5000);

    $tools_table = [
      '#type' => 'table',
      '#header' => [
        'Cutter',
        'Flush Valves',
        'Fittings Tool',
      ],
      '#rows' => [
        [
          $tools_quantity,
          $tools_quantity,
          $tools_quantity,
        ],
      ],
    ];

    $output['debug'] = [
      //'#markup' => '<pre>' . print_r($area_eng, TRUE) . print_r($area_val, TRUE) . '</pre><p>area: ' . (float) $area_eng->value . '; GPM: ' . $gpm . '</p>',
    ];

    $output['recommendations'] = [
      '#markup' => '<h2>Recommendations</h2><h3>Recommended Drip Tubing</h3>',
    ];

    $output['dripline'] = $dripline_table;

    $output['stakes_intro'] = [
      '#markup' => '<div class="results__item row1 first col-sm-4">' . '<h3>Stakes</h3>',
    ];

    $stakes_rows = [];
    $stakes_rows[] = [
      'part_number' => $stake_part,
      'stakes' => $stake_num . ' stake' . ($stake_num == 1 ? '' : 's'),
    ];
    $stakes_table = [
      '#type' => 'table',
      '#header' => ['Part Number', 'Number of Stakes'],
      '#rows' => $stakes_rows,
    ];
    $output['stakes'] = $stakes_table;

    $output['stakes_close'] = ['#markup' => '</div>'];

    $output['fittings_intro'] = [
      '#markup' => '<div class="results__item row1 first col-sm-4">' . '<h3>Fittings</h3>',
    ];

    $output['fittings'] = [
      '#markup' => '<p>On average, 40% of your total fittings will be Ts, 40% Elbows and 20% Couplings. Please use this as a general guideline when determining your specific configuration.</p>',
    ];

    $output['fittings_close'] = ['#markup' => '</div>'];

    $output['czk_intro'] = [
      '#markup' => '<div class="results__item row1 first col-sm-4">' . '<h3>Control Zone Kit</h3>',
    ];

    $gpm_header = [
      '#markup' => 'GPM', //'<span class="unit">' . $gpm_val->units . '</span>',
    ];

    $output['czk_close'] = ['#markup' => '</div>'];

    $output['czk_table'] = $czk_table;

    $output['tools_intro'] = [
      '#markup' => '<div class="results__item row1 first col-sm-4">' . '<h3>Tools</h3>',
    ];

    $output['tools_table'] = $tools_table;

    $output['tools_close'] = ['#markup' => '</div>'];

    $output['operational_indicator'] = [
      '#markup' => '<p>Operation Indicator: <span class="num-kits">' . $num_kits . '</span></p>',
    ];

    return $output;
  }

  protected function getConversion($system, $value, $units) {
    $class_value = 'value-' . strtolower($system);
    $class_units = 'units-' . strtolower($system);
    if ($system == 'Metric') {
      switch ($units) {
        case 'in':
          $units = 'cm';
          $value = $value * 2.54;
          break;
        case 'in/hr':
          $units = 'mm/hr';
          $value = $value * 25.4;
          break;
        case 'ft':
          $units = 'm';
          $value = $value * 0.3048;
          break;
        case 'GPH':
          $units = 'LPH';
          $value = $value * 3.78541;
          break;
        case 'GPM':
          $units = 'LPM';
          $value = $value * 3.78541;
          break;
        case 'psi':
          $units = 'bars';
          $value = $value * 0.0689476;
          break;
        case 'cm':
        case 'mm/hr':
        case 'm':
        case 'LPH':
        case 'LPM':
        case 'bars':
          $value = $value;
          break;
        default:
          $units = 'UNKNOWN';
          $value = 1;
      }
    }
    elseif ($system == 'English') {
      switch ($units) {
        case 'cm':
          $units = 'in';
          $value = $value / 2.54;
          break;
        case 'mm/hr':
          $units = 'in/hr';
          $value = $value / 25.4;
          break;
        case 'm':
          $units = 'ft';
          $value = $value / 0.3048;
          break;
        case 'LPH':
          $units = 'GPH';
          $value = $value / 3.78541;
          break;
        case 'LPM':
          $units = 'GPM';
          $value = $value / 3.78541;
          break;
        case 'bars':
          $units = 'psi';
          $value = $value / 0.0689476;
          break;
        case 'in':
        case 'in/hr':
        case 'ft':
        case 'GPH':
        case 'GPM':
        case 'psi':
          break;
        default:
          $units = 'UNKNOWN';
          $value = 1;
      }
    }
    $result = new \stdClass();
    $result->value = $value;
    $result->value_format = number_format($value, 2);
    $result->units = $units;
    $result->class_value = $class_value;
    $result->class_units = $class_units;
    return $result;
  }
}
