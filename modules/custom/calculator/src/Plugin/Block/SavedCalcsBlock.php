<?php

namespace Drupal\calculator\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Hello' Block.
 *
 * @Block(
 *   id = "saved_calcs_block",
 *   admin_label = @Translation("Saved Calculations"),
 *   category = @Translation("Rainbird Calculator"),
 * )
 */
class SavedCalcsBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $markup = '<p>You can save calculations after you run them, and they will display here temporarily.</p>';
    if (isset($_SESSION['calc'])) {
      foreach ($_SESSION['calc'] as $calc) {
        $query = http_build_query($calc);
        $markup .= '<h3>' . $calc['save_name'] . '</h3>';
        $markup .= '<p><a href="/calculator/' . $calc['num'] . '?' . $query . '">View or modify this saved search</a></p>';
      }
    }
    return array(
      '#cache' => ['max-age' => 0],
      '#markup' => $this->t($markup),
    );
  }

}
